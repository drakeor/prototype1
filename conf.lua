GameConfig = {
	ScreenWidth = 1280,
	ScreenHeight = 720
}

function love.conf(t)
	t.title = "Prototype Game #2"
	t.author = "Drakeor"
	t.url = "www.drakeor.com"
	t.console = true
	t.screen.width = GameConfig.ScreenWidth
	t.screen.height = GameConfig.ScreenHeight
	t.screen.fsaa = 8
end