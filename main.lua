-- Core stuff
require "scripts.graphics"
require "scripts.helper"

-- Libraries
Class = require "lib.class"
Gui = require 'lib.gui'
Camera = require 'lib.camera'
UnitTest = require "scripts.unittest"

-- Important Classes
require "scripts.classes"

-- Variables
EntityManager = EntityManagerClass()
MyPlayer = PlayerClass()
camera = Camera(100,100,1,0)
 
function love.load()
	loadImages()
	love.graphics.setBackgroundColor( 255, 255, 255 )
  
  EntityManager:add(CitycontrollerClass())
  
  background = BackgroundClass()
  EntityManager:add(background)
  
  for i=1,75 do 
	    EntityManager:add(ChickenClass())
	end

  EntityManager:add(MyPlayer)
  UnitTest.init()
end

function love.draw()
  UnitTest.draw()
	love.graphics.clear()
	love.graphics.setColor(255,255,255,255)
	
	love.graphics.setColor(0,0,0,255)
	love.graphics.print(" ",10,10)
	
	camera:attach()
	
	love.graphics.setColor(255,255,255,255)
	camx, camy = camera:pos()
	EntityManager:draw()
	Gui.core.draw()
	
	camera:detach()

	love.graphics.setColor(0,0,0,255)
	love.graphics.print("Money: " .. MyPlayer.money .. " || Chickens: " .. MyPlayer.chickens .. " Fillets: " .. MyPlayer.mcChickens,10,10)
	
end

function love.update(dt)
    UnitTest.update()
    camera:lookAt(math.floor(MyPlayer.x),math.floor(MyPlayer.y))
    EntityManager:update(dt)
end