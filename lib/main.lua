local gui = require 'gui'

function love.load()
	-- font must (for not) be set for the draw functions
	love.graphics.setFont(love.graphics.newFont(20))

	local sd = love.sound.newSoundData(44100, 44100, 16, 1)
	for i = 1,44100 do
		local phase = (i/44100)%1
		sd:setSample(i, math.sin(2 * math.pi * 220 * phase))
	end
	src = love.audio.newSource(sd)
	src:setLooping(true)
	src:setVolume(.5)
	love.audio.play(src)
end

-- slider state values. see the widget source for the meaning of these
local red   = {value = 0, max = 255}
local green = {value = 0, max = 255}
local blue  = {value = 0, max = 255}
local soundctl  = {value = {x = 1, y = .5}, min = {x=.5,y=0}, max = {x=1.5,y=1}}

function love.update(dt)
	local colorChanged = false

	-- gui.Button returns true if the button was clicked.
	if gui.Button("Black", 10,10, 150,30) then
		red.value   = 0
		green.value = 0
		blue.value  = 0
		colorChanged = true
	end

	if gui.Button("White", 170,10, 150,30) then
		red.value   = 255
		green.value = 255
		blue.value  = 255
		colorChanged = true
	end

	-- gui.Slider() returns true if the slider value was changed.
	-- Note:
	--    if gui.Slider(red, ...) or gui.Slider(green, ...) then does not work
	--    here, because if the first slider changes, the remaining ones will
	--    not be evaluated.
	local sliderChanged = gui.core.strictOr(
		gui.Slider(red,   40,80,  200,20),
		gui.Slider(green, 40,110, 200,20),
		gui.Slider(blue,  40,140, 200,20))

	if sliderChanged or colorChanged then
		love.graphics.setBackgroundColor(red.value, green.value, blue.value)
	end

	-- gui.Label() does not return anything.
	gui.Label('R', 10, 80)
	gui.Label('G', 10,110)
	gui.Label('B', 10,140)
	gui.Label(math.floor(red.value),   250, 80)
	gui.Label(math.floor(green.value), 250,110)
	gui.Label(math.floor(blue.value),  250,140)

	-- gui.Slider2D() returns true if (surpise!) the slider value was changed.
	if gui.Slider2D(soundctl, 560,10,200,200) then
		src:setPitch(soundctl.value.x)
		src:setVolume(1-soundctl.value.y)
	end
	gui.Label(('Freq: %.2f'):format(220*soundctl.value.x), 560, 216)
	gui.Label(('Vol: %d%%'):format(100-soundctl.value.y*100), 560, 240)
end

function love.draw()
	-- Draw the gui. Colors can be changed using the gui.core.color table:
	--
	--     gui.core.color.hot    = {fg = {r,g,b,a}, bg = {r,g,b,a}}
	--     gui.core.color.active = {fg = {r,g,b,a}, bg = {r,g,b,a}}
	--     gui.core.color.normal = {fg = {r,g,b,a}, bg = {r,g,b,a}}
	--
	-- or by passing different draw functions to the widgets.
	gui.core.draw()
end
