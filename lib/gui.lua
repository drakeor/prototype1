return {
	core     = require('lib/core'),
	Button   = require('lib/button'),
	Slider   = require('lib/slider'),
	Slider2D = require('lib/slider2d'),
	Label    = require('lib/label'),
}
