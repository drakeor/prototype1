local Class = require "lib.class"
local NewClass = Class{function(self)
	self.x = GameConfig.ScreenWidth
	self.y = GameConfig.ScreenHeight
	self.name = "townhall"
  self.class = "buildings"
	
  self.sizeX = 4
  self.sizeY = 4
  self.roadNodeX = 2
  self.roadNodeY = 4
  
  self.image = graphics.townhall
  self.originX = self.image:getWidth()/2
	self.originY = self.image:getHeight()/2
end}

function NewClass:update(dt)

end


function NewClass:draw()
  love.graphics.draw(graphics.townhall, self.x, self.y, 0, 1, 1, 0, 0)
end

return NewClass