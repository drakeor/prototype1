local Class = require "lib.class"
local NewClass = Class{function(self)
	self.x = GameConfig.ScreenWidth
	self.y = GameConfig.ScreenHeight
	self.name = "landplot"
  self.class = "buildings"
	
  self.sizeX = 1
  self.sizeY = 1
  
  self.image = graphics.townhall
  self.originX = self.image:getWidth()/2
	self.originY = self.image:getHeight()/2
end}

function NewClass:update(dt)

end


function NewClass:draw()
  love.graphics.draw(graphics.townhall, self.x, self.y, 0, 1, 1, self.originX, self.originY)
end

return NewClass