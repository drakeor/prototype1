local Class = {}

Class.horizontalCost = 10
Class.diagonalCost = 14

-- Finds the distance from the source to end block using the block method.
function Class:findBlockDistance(x1,y1,x2,y2)
  local dist = math.abs(y1 - y2) + math.abs(x1 - x2)
  return dist*10
end

-- Finds the real distance
function Class:findDistance(x1,y1,x2,y2)
  return (((x2-x1)^2+(y2-y1)^2)^0.5)*10
end

-- For debugging purposes
function Class:drawDetails(openlist, closedlist)
  

end

function Class:addToFreeList(controller, list, list2, x, y, parent)
  if(list2[x] == nil) then list2[x] = { } end
  if controller:isTileFree(x, y) then 
    if not list2[x][y] then
      self:addToList(list, x, y, parent)
    end
  end
end

-- Adds a block to a list
function Class:addToList(list, x, y, parent)
  if(list[x] == nil) then
    list[x] = {}
  end
  local data = { x = x, y = y, currentCost = 0, heuristicCost = 0, finalCost = 0, parent = parent }
  if(parent ~= nil) then
    data.currentCost = math.floor(self:findDistance(x, y, parent.x, parent.y)) + parent.currentCost
  end
  list[x][y] = data
end

-- Drops a block from the list
function Class:dropFromList(list, x, y)
  list[x][y] = nil
end

-- Switches data from one list to another
function Class:switchList(list, list2, x, y)
  local data = list[x][y]
  list[x][y] = nil
  if(list2[x] == nil) then
    list2[x] = {}
  end
  list2[x][y] = data
end


-- Gets all the blocks in the list
function Class:getAllInList(list, f)
  for k, v in pairs(list) do
      for ke, va in pairs(v) do 
        if(va ~= nil) then
          f(k, ke, va)
        end
      end
  end
end

function printTable (tbl, indent)
  if not indent then indent = 0 end
  for k, v in pairs(tbl) do
    formatting = string.rep("  ", indent) .. k .. ": "
    if type(v) == "table" then
      print(formatting)
      printTable(v, indent+1)
    elseif type(v) == 'boolean' then
      print(formatting .. tostring(v))		
    else
      print(formatting .. v)
    end
  end
end

function Class:calculate(controller, source_x, source_y, target_x, target_y)
  -- First off, initialize an open and closed list.
  local openList = { }
  local closedList = { }
  
  TargetX = target_x
  TargetY = target_y
  
  -- Add source to the open list
  self:addToList(openList, source_x, source_y)
  
  -- Iterate
  local endCounter = 0
  while(endCounter < 200) do
    endCounter = endCounter + 1
    local currentTile = nil
    local currentMinTile = 99999
    
    -- Get the tile with the least cost in the open list. Make it our current tile.
    self:getAllInList(openList, function(x, y, data)

        -- Generate hCost 
        if(data.heuristicCost == 0) then
          openList[x][y].heuristicCost = self:findBlockDistance(x, y, target_x, target_y)
          openList[x][y].finalCost = openList[x][y].heuristicCost + openList[x][y].currentCost
        end
        
        -- Take the lowest hCost
        if(openList[x][y].finalCost < currentMinTile) then
          currentTile = { x = x, y = y }
          currentMinTile = openList[x][y].finalCost
        end
        
        -- If the tile added was the ending tile, then stop.
        if(x == target_x) and (y == target_y) then
          currentTile = { x = x, y = y }
          
          -- Work backwards to give us a path.
          local finalList = {}
          finalList[x] = {}
          parent = openList[currentTile.x][currentTile.y].parent
          while(parent ~= nil) do
            finalList[x][y] = closedList[parent.x][parent.y]
            --closedList[parent.x][parent.y] = nil
            parent = closedList[parent.x][parent.y].parent
          end
          endCounter = 999999
          return finalList, openList, closedList
        end
      end)
    
    assert(currentTile ~= nil, "No valid path!")
    
    -- Push this square to the closed list
    self:switchList(openList, closedList, currentTile.x, currentTile.y)
    openList[currentTile.x][currentTile.y] = nil
    
    -- Add adjacent squares
    self:addToFreeList(controller, openList, closedList, currentTile.x-1, currentTile.y-1,  closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x-1, currentTile.y,    closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x-1, currentTile.y+1,  closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x,   currentTile.y-1,  closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x,   currentTile.y+1,  closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x+1, currentTile.y-1,  closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x+1, currentTile.y,    closedList[currentTile.x][currentTile.y])
    self:addToFreeList(controller, openList, closedList, currentTile.x+1, currentTile.y+1,  closedList[currentTile.x][currentTile.y])
    -- TODO: If its already on the open list, check to see if the current path is better. If so, change the parent to the current path.

  end
  
  return closedList, openList, closedList
end

return Class