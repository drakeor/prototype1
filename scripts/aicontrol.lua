-- EVERYTHING should follow this format

local Class = require "lib.class"
local NewClass = Class{function(self, parentClass)
	self.parent = parentClass
	self.name = "aicontrol"
	self.aiType = "unemployed"
	
	self.owner = 0
	self.parentHouse = 0
	
	self.targetID = 0
	self.speed = 200
	
	self.parent.image = "npc"
end}

function NewClass:checkType(wtype)
    if(self.aiType == wtype) then
        return true
    end
    return false
end

-- Turn this AI into a working-class citizen!
function NewClass:installWorkerRoutine(owner, building)
    local utest = owner.id + building.id
    self.owner = owner
    self.parentHouse = building
    self.aiType = "worker"
    self.parent.image = "worker"
end

-- Make the AI harvest chicken.
function NewClass:runTaskHarvestChildren()
    for v in EntityManager:allEntities() do
        if (v.name == "chicken") then
            if(math.random(1,3)) then 
                self.targetID = v.id
                break
            end
        end
    end
end

function NewClass:update(dt)

    -- We only want to work if we have a valid parent
    if self.parent ~= nil then
    
        -- Reset the target ID if the entity decides to die
        if EntityManager.buffer[self.targetID] == nil then
            self.targetID = 0    
        end
        
        -- So the NPC doesn't really have a target... We'll set one up for him/her.
        if self.targetID == 0 then

            
        -- We have a target! Lets start moving there.    
        else
            local foundAngle = findAngleInRadians(self.parent.x, self.parent.y, EntityManager.buffer[self.targetID].x, EntityManager.buffer[self.targetID].y)
            self.parent:move(foundAngle)
        end   
        
        -- Handle collisions ( I want to replace this with event based stuff later)
        for v in EntityManager:allEntities() do
            if checkCircularCollision(self.parent.x, self.parent.y, v.x, v.y, 32, 32) then
            
                
            end
        end
        
        
    end
end


function NewClass:draw()

end

return NewClass