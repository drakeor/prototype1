local Class = require "lib.class"
local MarketClass = Class{function(self)
	self.x = (GameConfig.ScreenWidth*1.5)
	self.y = (GameConfig.ScreenHeight*1.5)
	self.name = "market"
	self.originX = 128
	self.originY = 128
	self.marketFunds = 10000
		
	self.chickenPrice = 150
	self.chickenFluxPrice = 1
    self.mcChickenPrice = 300
    self.mcChickenFluxPrice = 5
		
end}

function MarketClass:update(dt)
	local chickenSellPrice = (self.chickenPrice*0.95)
	local mcChickenSellPrice = (self.mcChickenPrice*0.95)
		
    if Gui.Button("Buy Chicken $" .. self.chickenPrice, self.x-self.originX, self.y-self.originY, 120,30) then
        if(MyPlayer.money > self.chickenPrice) then
            MyPlayer.money = MyPlayer.money - self.chickenPrice
            self.chickenPrice = self.chickenPrice+self.chickenFluxPrice
            MyPlayer.chickens = MyPlayer.chickens + 1
        end
    end
    
    if Gui.Button("Sell Chicken $" .. chickenSellPrice, self.x-self.originX+125, self.y-self.originY, 120,30) then
        if(MyPlayer.chickens > 0) then 
            MyPlayer.money = self:sellChicken(MyPlayer.money)
            MyPlayer.chickens = MyPlayer.chickens - 1
        end
    end
    
    if Gui.Button("Buy Fillet $" .. self.mcChickenPrice, self.x-self.originX, self.y-self.originY-40, 120,30) then
        if(MyPlayer.money > self.mcChickenPrice) then 
            MyPlayer.money = MyPlayer.money - self.mcChickenPrice
            self.mcChickenPrice = self.mcChickenPrice+self.mcChickenFluxPrice
            MyPlayer.mcChickens = MyPlayer.mcChickens + 1
        end
    end
    
    if Gui.Button("Sell Fillet $" .. mcChickenSellPrice, self.x-self.originX+125, self.y-self.originY-40, 120,30) then
        if(MyPlayer.mcChickens > 0) then 
            MyPlayer.money = self:sellMcChicken(MyPlayer.money)
            MyPlayer.mcChickens = MyPlayer.mcChickens - 1
        end
    end
    
    if Gui.Button("Buy Manufactory $1500", self.x-self.originX, self.y-self.originY-80, 250,30) then
        if(MyPlayer.money > 1500) then 
            MyPlayer.money = MyPlayer.money - 1500
            NewManufactory = ManufactoryClass()
            NewManufactory.owner = MyPlayer
            EntityManager:add(NewManufactory)
        end
    end
end

function MarketClass:sellChicken(money)
    local chickenSellPrice = (self.chickenPrice*0.95)
    self.chickenPrice = self.chickenPrice-self.chickenFluxPrice
    money = money + chickenSellPrice
    return money
end

function MarketClass:sellMcChicken(money)
    local chickenSellPrice = (self.mcChickenPrice*0.95)
    self.mcChickenPrice = self.mcChickenPrice-self.mcChickenFluxPrice
    money = money + chickenSellPrice
    return money
end

function MarketClass:draw()
	love.graphics.draw(graphics.market, self.x, self.y, 0, 1, 1, self.originX, self.originY)
end

return MarketClass