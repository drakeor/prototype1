-- EVERYTHING should follow this format

local Class = require "lib.class"
local NewClass = Class{function(self)
	self.x = GameConfig.ScreenWidth
	self.y = GameConfig.ScreenHeight
	self.name = "waterblock"
	
  self.image = graphics.water
  self.originX = self.image:getWidth()/2
	self.originY = self.image:getHeight()/2
end}

function NewClass:update(dt)

end


function NewClass:draw()
  love.graphics.draw(graphics.water, self.x, self.y, 0, 1, 1, 0, 0)
end

return NewClass