local Class = require "lib.class"
local NewClass = Class{function(self)
	self.x = 0
	self.y = 0
  self.z = -1
	self.name = "background"
	
  self.image = graphics.background;
  self.image:setWrap( "repeat", "repeat" )
  self.imageQuad = love.graphics.newQuad(0,0, GameConfig.ScreenWidth, GameConfig.ScreenHeight, graphics.background:getWidth(), self.image:getHeight())
  
end}

function NewClass:update(dt)

end


function NewClass:draw()
  local camx, camy = camera:pos()
  local realx, realy = camera:worldCoords(0,0)
  self.imageQuad:setViewport(camx, camy, GameConfig.ScreenWidth, GameConfig.ScreenHeight)
  love.graphics.drawq(self.image, self.imageQuad, realx, realy)
end

return NewClass