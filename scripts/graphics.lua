graphics = {}

require "lib.slam"

function loadImages()
	graphics = {
		player = love.graphics.newImage("textures/player.png"),
		npc = love.graphics.newImage("textures/npc.png"),
		market = love.graphics.newImage("textures/market.png"),
		chicken = love.graphics.newImage("textures/chicken.png"),
		manufactory = love.graphics.newImage("textures/manufactory.png"),
		worker = love.graphics.newImage("textures/worker.png"),
		wtf = love.graphics.newImage("textures/wtf.png"),
		tileset = love.graphics.newImage("textures/tilemap.png"),
    background = love.graphics.newImage("textures/grass.png"),
    townhall = love.graphics.newImage("textures/townhall.png"),
    landplot = love.graphics.newImage("textures/citylandplot.png"),
    workhouse = love.graphics.newImage("textures/workhouse.png"),
    water = love.graphics.newImage("textures/water.png"),
    road = love.graphics.newImage("textures/grass-tinted.png")
	}
  
    sound = love.audio.newSource("chicken.wav", "STATIC")
end