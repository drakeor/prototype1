-- This is run AFTER all of the clases and verifies data.

local NewClass = {}

function NewClass:init()
end

function NewClass:update(dt)
  EntityManager:allEntities(function(v)
      
      -- Simple Class checks
      assert(v ~= nil, "EntityManager is leaking nil classes!")
      assert(v.name ~= nil, "Invalid entity in EntityManager")
      assert(v.name ~= "newclass", "One of the entities never had their name set.")

      -- Building Class checks
      if(v.class == "building") then
        assert(v.sizeX ~= nil, "The building named " .. v.name .. " does not have a sizeX attribute!")
        assert(v.sizeY ~= nil, "The building named " .. v.name .. " does not have a sizeY attribute!")
      end
      
    end)
end


function NewClass:draw()
  
end

return NewClass