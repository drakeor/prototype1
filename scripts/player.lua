local Class = require "lib.class"
local PlayerClass = Class{function(self)
	self.x = GameConfig.ScreenWidth
	self.y = GameConfig.ScreenHeight
  self.z = 1
	self.originX = 32
	self.originY = 32
	self.speed = 300
	self.money = 2000
	self.name = "player"
	self.aiControl = nil
	self.aiClass = nil
	self.dt = 0
	self.image = "player"
	
	self.inventory = {}
	self.inventorylimit = 2
	
	self.chickens = 0
	self.mcChickens = 0
end}

-- Note that the angle needs to be in radians.
function PlayerClass:move(angle, speed)
    local fspeed = self.speed
    if(speed ~= nil) then 
        fspeed = speed
    end
    self.x = self.x + math.cos(angle) * fspeed*self.dt
    self.y = self.y + math.sin(angle) * fspeed*self.dt
end

-- Turn this AI into a free citizen
function PlayerClass:enableAI()
    self.aiClass = require "scripts.aicontrol"
    self.aiControl = self.aiClass(self)
end

function PlayerClass:update(dt)
    -- This is for internal functions
    self.dt = dt
    
    -- If this is not the AI and its the current player, then allow him or her to move
    if (aiControl == nil) and (self.id == MyPlayer.id) then
        if love.keyboard.isDown("w") then
            self:move(4.712385)
        end 
        if love.keyboard.isDown("a") then
            self:move(3.14159)
        end
        if love.keyboard.isDown("s") then
            self:move(1.570795)
        end 
        if love.keyboard.isDown("d") then
            self:move(0)
        end 
        if love.keyboard.isDown("l") then
            EntityManager:add(ChickenClass())
        end
        if love.keyboard.isDown(" ") then
          camera:zoomTo(0.25)
        else
          camera:zoomTo(1)
        end
    end
    
    -- If this is an AI, update them
    if(self.aiControl ~= nil) then
        self.aiControl:update()
    end
end

function PlayerClass:draw()
	love.graphics.draw(graphics[self.image], self.x, self.y, 0, 1, 1, self.originX, self.originY)
	if(self.aiControl ~= nil) then
	    -- Show the current funds of the NPCs for debug purposes
        love.graphics.setColor(0,0,0,255)
        Gui.Label("Money: " .. math.ceil(self.money), self.x-16, self.y-16)
        love.graphics.setColor(255,255,255,255)
        self.aiControl:draw()
    end
end

function PlayerClass:hasFreeSpace()
  return (#self.inventory < self.inventorylimit)
end

function PlayerClass:addToInventory(item)
  
end

return PlayerClass