function checkCircularCollision(ax, ay, bx, by, ar, br)
    local dx = bx - ax
    local dy = by - ay
    return dx^2 + dy^2 < (ar + br)^2
end

function findAngleInDegrees(x1, y1, x2, y2)
    local t = -math.deg(math.atan2(y2-y1, x2-x1))
    if t < 0 then t = t + 360 end;
    return t;
end

function findAngleInRadians(x1, y1, x2, y2)
    local t = math.atan2(y2-y1, x2-x1)
    return t;
end