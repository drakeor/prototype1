-- EVERYTHING should follow this format

local Class = require "lib.class"
local Manufactory = Class{function(self)
	self.x = math.random(350,800)+(GameConfig.ScreenWidth/2)
	self.y = math.random(-500,500)+(GameConfig.ScreenHeight/2)
	self.name = "manufactory"
	self.owner = 0
	self.originX = 128
	self.originY = 128
	self.progress = 0
	
	self.chickens = 0
	self.patties = 0
	self.hasWorker = false
end}

function Manufactory:hireWorker()
    self.hasWorker = true
    NewWorker = PlayerClass()
    NewWorker.x = self.x
    NewWorker.y = self.y
    NewWorker:enableAI()
    NewWorker.aiControl:installWorkerRoutine(self.owner,self)
    EntityManager:add(NewWorker)
end

function Manufactory:update(dt)
    
    love.graphics.setColor(0,0,0,255)
    Gui.Label("Progress: " .. math.ceil(self.progress) .. "%", self.x-self.originX, self.y+self.originY)
    love.graphics.setColor(255,255,255,255)
	
	local costOfWorker = 1000
	if not self.hasWorker then
	    costOfWorker = 0
    end
	
	if(self.owner.id == MyPlayer.id) then
	    if Gui.Button("Hire Worker $" .. costOfWorker, self.x-self.originX, self.y-self.originY-40, 120,30) then
            if(MyPlayer.money > costOfWorker) then
                MyPlayer.money = MyPlayer.money - costOfWorker
                self:hireWorker()
            end
        end
	    
        if Gui.Button("Deposit chicken (" .. self.chickens .. ")", self.x-self.originX, self.y-self.originY, 120,30) then
            if(MyPlayer.chickens > 0) then
                MyPlayer.chickens = MyPlayer.chickens - 1
                self.chickens = self.chickens + 1
            end
        end
        
        if Gui.Button("Withdraw fillet (" .. self.patties .. ")", self.x-self.originX+125, self.y-self.originY, 120,30) then
            if(self.patties > 0) then
                MyPlayer.mcChickens = MyPlayer.mcChickens + 1
                self.patties = self.patties - 1
            end
        end
    end
    
    if(self.chickens > 0) then
        self.progress = self.progress + dt*12
        if(self.progress > 100) then
            self.chickens = self.chickens - 1
            self.patties = self.patties + 1
            self.progress = 0
        end    
    end
end


function Manufactory:draw()
    love.graphics.draw(graphics.manufactory, self.x, self.y, 0, 1, 1, self.originX, self.originY)
end

return Manufactory