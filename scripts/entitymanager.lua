local Class = require "lib.class"
local EntityManager = Class{function(self)
    self.buffer = {}
    self.bufferIndex = 0
    self.zDrawEnabled = true
end}

-- Adds an entity to the entitymanager which will automatically be drawn and updated. It sets an id attribute in the entity.
function EntityManager:add(ent)
    self.bufferIndex = self.bufferIndex + 1
    self.buffer[self.bufferIndex] = ent
    ent.id = self.bufferIndex
    return self.bufferIndex
end

-- This is an iterative function that returns all Non-nil entities. You can also pass a function if you want.
function EntityManager:allEntities(f)
    if f ~= nil then
        for k, v in pairs(self.buffer) do
            if v ~= nil then
                f(v)
            end
        end
    else
       local i = 0
       local len = #self.buffer
       return function()
                i = i + 1
                while (i <= len+1) and (self.buffer[i] == nil) do
                    i = i + 1
                end
                if i <= len then
                    return self.buffer[i]
                end
            end
    end
end

function EntityManager:get(id)
    return self.buffer[id]
end

-- Deletes an entity and hopefully the internal lua garbage collector is smart enough to pick off the object.
function EntityManager:remove(pos)
    self.buffer[pos] = nil
end

function EntityManager:draw()
  if(self.zDrawEnabled) then
    local allObjs = { }
    self:allEntities(function(v)
          table.insert(allObjs, v)
        end)
    table.sort(allObjs, function(a, b)
          az = 0
          bz = 0
          if(a.z ~= nil) then az = a.z end
          if(b.z ~= nil) then bz = b.z end
          if(az < bz) then 
            return true
          else
            return false
          end
        end)
    for k, v in pairs(allObjs) do
        v:draw()
    end
  else
    self:allEntities( function(v)
        v:draw()
    end)
  end
end

function EntityManager:update(dt)
    self:allEntities( function(v)
        v:update(dt)
    end)
end

return EntityManager