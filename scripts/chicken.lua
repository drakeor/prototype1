local Class = require "lib.class"
local ChickenClass = Class{function(self)
	self.x = (math.random()*GameConfig.ScreenWidth*2)
	self.y = (math.random()*GameConfig.ScreenHeight*2)
	self.anchorX = self.x
	self.anchorY = self.y
	self.speed = 70
	self.meat = 2
	self.name = "chicken"
	
	self.isWandering = false
	self.wanderXSpeed = 0
	self.wanderYSpeed = 0
	self.wanderRestTime = 0
	self.image = "chicken"
  self.instance = nil
	if(math.random(1, 30) == 5) then
	    self.image = "wtf"
	end
end}

function ChickenClass:update(dt)
  
    -- Chickens. Masters of wandering aimlessly
    if(self.wanderRestTime <= 0) then
        if(self.isWandering == true) then
            self.wanderXSpeed = 0
	        self.wanderYSpeed = 0
	        self.isWandering = false
        end
        if(math.random(1, 7) == 5) then
            self.wanderXSpeed = ((math.random()*self.speed*2)-self.speed)
            self.wanderYSpeed = ((math.random()*self.speed*2)-self.speed)
            self.wanderRestTime = math.random()
            
            self.isWandering = true
            
            --self.instance = sound:play()
          else
            self.wanderRestTime = math.random()+0.5
        end
    end
    self.wanderRestTime = self.wanderRestTime - dt
    self.x = self.x + self.wanderXSpeed*dt
    self.y = self.y + self.wanderYSpeed*dt
    
    -- GTFO of the market Chicken!
    for v in EntityManager:allEntities() do
        if(v.name == "market") then 
            if checkCircularCollision(self.x, self.y, v.x, v.y, 64, 128) then
                self.wanderXSpeed = ((math.random()*self.speed*5)-self.speed)
                self.wanderYSpeed = ((math.random()*self.speed*5)-self.speed)
                self.wanderRestTime = math.random()
                self.isWandering = true
            end
        end
    end    
end

function ChickenClass:setPosition(x, y)
    self.x = x
    self.anchorx = x
    self.y = y
    self.anchory = y
end

function ChickenClass:draw()
	love.graphics.draw(graphics[self.image], self.x, self.y)
end

return ChickenClass