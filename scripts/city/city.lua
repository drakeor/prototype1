-- EVERYTHING should follow this format

local Class = require "lib.class"
local NewClass = Class{function(self)
	self.x = GameConfig.ScreenWidth
	self.y = GameConfig.ScreenHeight
  self.tileX = 0
  self.tileY = 0
  self.cityBuildings = { }
  
	self.name = "city.city"
	self.parent = nil
  self.hasCapital = false
  
end}

function NewClass:buildingExists(ident)
  for v in self.cityBuildings do
    if(v.name == ident) then
      return true;
    end
  end
end


function NewClass:addBuilding(x, y, building)
  EntityManager:add(building)
  --assert((x+building.sizeX) <= self.sizeX, "City: Recieved " ..(x+building.sizeX) .. " and max is " .. self.sizeX)
  --assert((y+building.sizeY) <= self.sizeY, "City: Recieved " .. (y+building.sizeY) .. " and max is " .. self.sizeY)
  local realX = (self.parent.tileSizeX * self.x) + (self.parent.tileSizeX * x) 
  local realY = (self.parent.tileSizeY * self.y) + (self.parent.tileSizeY * y)
  self.parent:claimArea(building, x + self.x, y + self.y, building.sizeX, building.sizeY)
  building.x = realX
  building.y = realY
  building.tilex = x + self.x
  building.tiley = y + self.y
  building.parent = self
  table.insert(self.cityBuildings, building)
end


function NewClass:update(dt)
  assert(self.parent ~= nil, "Uh oh. This should have a parent city controller.")
  if not self.hasCapital then
    self.capital = TownhallClass()
    --self.capital.x = self.x
    --self.capital.y = self.y
    self:addBuilding(4,4,self.capital)
    
    local workHut = WorkhouseClass()
    --workHut.x = self.x+256
    --workHut.y = self.y+256
    self:addBuilding(9,4,workHut)
    
    self.hasCapital = true
  end
end



function NewClass:draw()

end

return NewClass