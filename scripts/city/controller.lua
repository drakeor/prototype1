-- This important class is generally placed in places wehere we want cities to be built.
-- It will spawn a town hall (Where the controller is) and a market and accumulate fundings and citizens
-- It will also dynamically zone the city


--[[
PseudoCode:
  
]]--

local Class = require "lib.class"
local NewClass = Class{function(self)
	self.x = 0
	self.y = 0
	self.name = "city.controller"
  self.city = { }
	self.land = { }
  self.cityBuildings = { }
  
  self.capital = nil
  self:reallocateLand(64,64)
  self.tileSizeX = 64
  self.tileSizeY = 64
  self.hasCapital = false
  
  self.path = require "scripts.ai.findpath"
end}

function NewClass:reallocateLand(sizeX, sizeY)
  self.sizeX = sizeX
  self.sizeY = sizeY
  for i = 1,sizeX do
    self.land[i] = { }
    for j = 1, sizeY do
      self.land[i][j] = 0
    end
  end
  
end

function NewClass:interlinkCapitals()
  
end

function NewClass:isTileFree(x, y)
  if(x > 0) and (y > 0) then
  return (type(self.land[x][y]) == "number")
  else return false
    end
end


function NewClass:newCity(x,y)
  -- REMEMBER THESE ARE IN TILE X/Y
  city = CityClass()
  city.x = x
  city.y = y
  city.parent = self
  EntityManager:add(city)
  table.insert(self.city, city)
end

function NewClass:canPlaceBuilding()
  return true
end

function NewClass:addCity(x, y, city)
  -- If not within boundarys of other city..
  EntityManager:add(city)
  
end

 function NewClass:claimArea(ent, x, y, sizeX, sizeY)
 if(sizeX > 1) or (sizeY > 1) then
    for i = x, (x+sizeX-1) do
      for j = y, (y+sizeY-1) do
        self.land[i][j] = ent
      end
    end
  else
    self.land[x][y] = ent
  end
  return true
 end
 
function NewClass:pasteEntityToBlock(entClass, x, y, sizeX, sizeY)
  if(sizeX > 1) or (sizeY > 1) then
    for i = x, (x+sizeX-1) do
      for j = y, (y+sizeY-1) do
        local ent = entClass()
        self.land[i][j] = ent
        ent.x = self.tileSizeX * i
        ent.y = self.tileSizeY * j
        EntityManager:add(ent)
      end
    end
  else
    local ent = entClass()
    self.land[x][y] = ent
    ent.x = self.tileSizeX * x
    ent.y = self.tileSizeY * y
    EntityManager:add(ent)
  end
end

function NewClass:isSpaceFree(x, y, sizeX, sizeY)
  if(sizeX > 1) or (sizeY > 1) then
    for i = x, (x+sizeX-1) do
      for j = y, (y+sizeY-1) do
        if(self.land[i][j] ~= 0) then
          return false
        end
      end
    end
  else
    if(self.land[x][y] ~= 0) then
      return false
    end
  end
  return true
end

function NewClass:update(dt)
  if self.hasCapital == false then
    
    self:pasteEntityToBlock(WaterblockClass, 15, 7, 3, 10)
    --self:pasteEntityToBlock(WaterblockClass, 10, 17, 7, 1)
    self:newCity(0,0)
    
    self:newCity(24,10)
    
    self.hasCapital = true
  end
  if love.keyboard.isDown("r") then
    self.finalList, self.openList, self.closedList = self.path:calculate(self, 8, 5, 30, 18)
  end
end


function NewClass:draw()
  for i = 1, self.sizeX do
    for j = 1, self.sizeY do
      if (type(self.land[i][j]) == "number") then 
        if(self.land[i][j] == 1) then
          love.graphics.draw(graphics.road, self.x+(self.tileSizeX*i), self.y+(self.tileSizeY*j), 0, 1, 1, 0, 0)
        else
          love.graphics.draw(graphics.landplot, self.x+(self.tileSizeX*i), self.y+(self.tileSizeY*j), 0, 1, 1, 0, 0)
        end
      end
    end
  end
  
  -- Waypointing debug.
  --love.graphics.setColor(125,255,128,255)
  --love.graphics.print("[GOAL]", 24*64, 10*64)
  --love.graphics.setColor(255,255,255,255)
  
  if(self.openList ~= nil) then
    self.path:getAllInList(self.openList, function(x, y, data)
      love.graphics.setColor(10,255,128,255)
      love.graphics.print(data.currentCost .. "\n" .. data.heuristicCost .. "\n" .. data.finalCost, x*64, y*64)
      love.graphics.setColor(255,255,255,255)
    end)

    self.path:getAllInList(self.closedList, function(x, y, data)
      love.graphics.setColor(64,128,0,255)
      love.graphics.print(data.currentCost .. "\n" .. data.heuristicCost .. "\n" .. data.finalCost, x*64, y*64)
      love.graphics.setColor(255,255,255,255)
    end)
  
  self.path:getAllInList(self.finalList, function(x, y, data)
      love.graphics.setColor(32,32,32,255)
      love.graphics.print(data.currentCost .. "\n" .. data.heuristicCost .. "\n" .. data.finalCost, x*64, y*64)
      love.graphics.setColor(255,255,255,255)
    end)
  end
end

return NewClass